// import coursesData from "../data/coursesData";
import { useEffect, useState } from "react";

import ProductCard from "../components/ProductCard";

export default function Products(){


	// State that will be used to store the courses retrieved from the database
	const [products, setProducts] = useState([]);

	// Retrieve the courses from the database upon initial render of the Course component.
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setProducts(data.map(products =>{
				return(
					<ProductCard key={products._id} productProp={products} />
				)
			}))

		})

	}, [])


	return(
		<>
			<h1 className="text-center">Menu</h1>
			{products}
		</>

	)
}