import {Row, Col, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Highlights(){

	return(
		
		<Row className="mt-3 mb-3">
			<h2 className="text-center">Featured Products</h2>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h3 className="text-center">Palabok</h3>
		                </Card.Title>
		                <Card.Text>
		                    A traditional Filipino dish of rice noodles drenched in creamy shrimp paste sauce and complemented with various toppings: hard-boiled eggs, pork chicharon, shrimp, pork, fish flakes and scallions
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h3 className="text-center">Spring Rolls (Lumpiang Sariwa)</h3>
		                </Card.Title>
		                <Card.Text>
		                    Enjoy a vegetarian, super light and healthy appetizer wrapped in traditional egg wrapper
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h3 className="text-center">Cheesy Baked Macaroni</h3>
		                </Card.Title>
		                <Card.Text>
		                    Lavishly coated with tasty meat sauce, topped with heavenly cheese topping, and backed to perfection.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>

	)
}